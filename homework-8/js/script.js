$('.page-nav>a').click(function (event) {
    event.preventDefault();
    $('html,body').animate({
        scrollTop: $($(this).attr('href')).offset().top},1000);
});
/*----------------------------------------------*/
let $buttonUp=$('<button class="button-up">Up</button>');
const screenHeight = $(window).height();
$buttonUp.click(function () {
    $('html').animate({
        scrollTop: 0
    },2000)
});

$(window).scroll(function () {
    if($(window).scrollTop()>=screenHeight){
        $buttonUp.show();
        $('script:first').before($buttonUp);
    } else{
        $buttonUp.hide();
    }
});