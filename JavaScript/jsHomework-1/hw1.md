1.Variable var is function scoped(available inside entire function), but let is block scoped(available inside the block,e.g.,for or if).
2.When we try to access a variable var before it's declared, variable will be undefined.
But when we try to access a variable let before it's declared,there will be an error.
3.const almost the same as let, but const can't be reassigned.