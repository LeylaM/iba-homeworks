const titles=document.querySelectorAll('.tabs>li');
const content=document.querySelectorAll('.tabs-content>li');
titles.forEach(function (title) {
    title.addEventListener('click',function (event) {
        titles.forEach(function (title) {
            title.classList.remove('active');
        });
        content.forEach(function (item) {
            item.classList.remove('active');
        });
        event.target.classList.add('active');
        const attrValue= event.target.getAttribute('data-title');
        document.querySelector(`[data-content="${attrValue}"]`).classList.add('active');
    })
});