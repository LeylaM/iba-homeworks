const buttons=document.querySelectorAll('.btn');

function removeBlue(){
    buttons.forEach(function (button) {
        button.classList.remove('blue-btn');
    });
}

document.addEventListener('keyup',function (event) {
    buttons.forEach(function (button) {
        if(event.code===button.dataset.code){
            removeBlue();
            button.classList.add('blue-btn');
        }
    })
});
