let img = document.querySelectorAll('.image-to-show');
let i=0;
function nextImage() {
        if(i<img.length-1){
            img[i].classList.remove('shown');
            img[i+1].classList.add('shown');
            i++;
        }else{
            img[img.length-1].classList.remove('shown');
            i=0;
            img[i].classList.add('shown');
            nextImage();
        }
}
let interval=setInterval(nextImage,10000);

let stopBtn=document.querySelector('.stop-btn');
stopBtn.addEventListener('click',function () {
    clearInterval(interval);
});

let resumeBtn=document.querySelector('.resume-btn');
resumeBtn.addEventListener('click',function () {
    interval=setInterval(nextImage,10000);
});