let btn=document.querySelector('.change-theme');
let upperMenu=document.querySelector('.upper-menu');
let upperMenuItems=document.querySelectorAll('.upper-menu-element');
let blueText=document.querySelector('.blue-text');
let logoText=document.querySelector('.logo-text');
let sideMenu=document.querySelector('.side-menu');
let sideMenuItems=document.querySelectorAll('.side-menu-element');
let footer=document.querySelector('.footer');
let footerMenuItems=document.querySelectorAll('.footer-menu-element');
let imageBlock=document.querySelector('.picture-block');
function changeColorTheme() {
    upperMenu.classList.toggle('upper-menu-new');
    upperMenuItems.forEach(function (elem) {
        elem.classList.toggle('upper-menu-element-new');
    });
    blueText.classList.toggle('pink-text');
    logoText.classList.toggle('new-blue-text');
    sideMenu.classList.toggle('side-menu-new');
    sideMenuItems.forEach(function (elem) {
        elem.classList.toggle('side-menu-element-new');
    });
    footer.classList.toggle('footer-new');
    footerMenuItems.forEach(function (elem) {
        elem.classList.toggle('footer-menu-element-new');
    });
    imageBlock.classList.toggle('picture-block-new');
}



btn.addEventListener('click',function () {
    let newTheme=btn.classList.toggle('change-theme-new');
    changeColorTheme();
    localStorage.setItem('new-theme',`${newTheme}`);
});


if(JSON.parse(localStorage.getItem('new-theme'))){
    newTheme=btn.classList.toggle('change-theme-new');
  changeColorTheme();
}
