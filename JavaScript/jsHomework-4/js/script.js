function createNewUser(name,surname) {
    const newUser={
        firstName: name,
        lastName: surname,
        getLogin: function () {
           return (this.firstName[0] + this.lastName).toLowerCase();
        }
    };
    return newUser;
}
const user=createNewUser("Leyla", "Mammadli");
console.log(user);
console.log(user.getLogin());