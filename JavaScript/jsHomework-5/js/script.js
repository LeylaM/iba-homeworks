function createNewUser(name,surname,dateOfBirth) {
    const newUser={
        firstName: name,
        lastName: surname,
        birthday:dateOfBirth,
        getLogin: function () {
            return (this.firstName[0] + this.lastName).toLowerCase();
        },
        getAge: function () {
            let str=dateOfBirth.split('.');
            let date=new Date();
            date.setFullYear(str[2]);
            date.setMonth(parseInt(str[1])-1);
            date.setDate(str[0]);
            let currentDate=new Date();
            let currentYear=currentDate.getFullYear();
            let currentMonth=currentDate.getMonth()+1;
            let currentDay=currentDate.getDate();
            if(currentMonth<str[1]){
                return currentYear-str[2]-1;
            }else if(currentMonth>str[1]){
                return currentYear-str[2];
            }else{
                if(currentDay<str[0]){
                    return currentYear-str[2]-1;
                }else if(currentDay>=str[0]){
                    return currentYear-str[2];
                }
            }
        },
        getPassword:function () {
            let str=dateOfBirth.split('.');
            return this.firstName[0].toUpperCase()+this.lastName.toLowerCase()+str[2];
        }
    };
    return newUser;
}

const user=createNewUser('Leyla', 'Mammadli', '21.12.1997');
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());


