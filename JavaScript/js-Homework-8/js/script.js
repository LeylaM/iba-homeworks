const inputPrice=document.querySelector(".inputText");
const inputLabel=document.querySelector(".inputLabel");

window.addEventListener("load", function() {
    inputPrice.style.display='inline-block';
});

inputPrice.addEventListener('focus',function () {
   inputPrice.style.border='green 2px solid';
});
const enteredPrice=document.createElement('div');
const spanText=document.createElement('span');
const cancelBtn=document.createElement('button');
const redText=document.createElement('div');
cancelBtn.textContent='X';
enteredPrice.appendChild(spanText);
enteredPrice.appendChild(cancelBtn);
inputPrice.addEventListener('blur',function () {
    inputPrice.style.border='lightgrey 1px solid';
    if(parseFloat(inputPrice.value)>0){
        inputLabel.before(enteredPrice);
        spanText.textContent=`Current price: ${inputPrice.value} `;
        inputPrice.style.border='green 2px solid';
        inputPrice.style.color='green';
        redText.remove();
    }else if(parseFloat(inputPrice.value)<=0){
        inputPrice.style.border='red 2px solid';
        inputLabel.after(redText);
        redText.style.color='red';
        inputPrice.style.color='red';
        redText.textContent='Please enter correct price';
        enteredPrice.remove();
    } else if(inputPrice.value===''){
        enteredPrice.remove();
        redText.remove();
    }
});
cancelBtn.addEventListener('click',function () {
   enteredPrice.remove();
   inputPrice.value='';
});