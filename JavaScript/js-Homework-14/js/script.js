const $content=$('.tabs-content');

$('[data-title]').on('click', function (event) {
    $(this).addClass('active').siblings('[data-title]').removeClass('active');
    $content.children('li[data-content]').removeClass('active');
    $content.children('li[data-content=' + $(this).data('title') + ']').addClass('active');
});